package com.cfa.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "sumServlet",urlPatterns = {"/sum"})
public class Sum extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
            IOException {
        this.doRequest(request,response);
    }
    protected void doPost(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException,
            IOException {
       this.doRequest(request,response);
    }
    private void doRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String num1 = request.getParameter("num1");
        String num2 = request.getParameter("num2");
        int sum = Integer.parseInt(num1) + Integer.parseInt(num2);

        out.println("<html><body>"+
                "sum of" + num1 + num2+"equals" + String.valueOf(sum));
    }
}

